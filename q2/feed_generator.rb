class FeedGenerator
  def initialize(user)
    @user = user
  end

  def retrieve
    #Feed ordering explanation:
    #if user just upvoted some book it should be displayed on his feed at the top.
    #if user just started following some author all his books will be at the top of user's feed.
    @feed ||= begin
      fetch_books
        .order('modified_at desc')
    end
  end

  def refresh(id)
    last_book_displayed = fetch_books.find_by(id: id)
    @refresh_books ||= begin
      fetch_books
        .where('modified_at > ?', last_book_displayed.modified_at)
        .order('modified_at desc')
    end
  end

  private

  def fetch_books
    @books ||= Book.left_joins(:upvotes, [author: :follows]).where(
      'follows.user_id = :user_id OR upvotes.user_id = :user_id',
      user_id: @user.id).
    select('books.*, coalesce(upvotes.created_at, follows.created_at) as modified_at')
  end
end
