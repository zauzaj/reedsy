Recommendation system

As we have books, genres and users who can upvote/like books, at the beginning I would go forward with **collaborative filtering** approach. It is straightforward and clean recommendation algorithm where everything rely on interaction between other users.
The recommendations are based on upvotes provided by other users who mostly read and like books from the same genre as the user.
In simple words, we're finding users with same or similar taste based on books and genres they all likes, then calculating *weights* for most "liked" books (probably only in genres that active user likes the most) and recommend books to the active user (only those that he didn't upvote yet).

Possible issue, with this approach, would be exponentially increasing calculation time as number of users is increasing at the time, that could lead us to some performance issues.
Some of the possible technical improvements would be introducing NoSql storage and asynchronous job(Sidekiq) that would always keep that storage synced.
Or, to queue recommendations and refresh them in background based on some time interval using RabbitMQ for example.

Beside technical improvements, there are also different algorithms improvements, like we don't need to deal with all users but maybe only that are following same authors, or if there are many of similar users we can take top 10.

Also there are another algorithms that could be implemented, like *matrix factorization* but that's more complex and it might be overkill at the beginning.


