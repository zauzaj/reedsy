class Recommender
  def self.call(user)
    new(user).call
  end

  def initialize(user)
    @user = user
  end

  def call
    return Book.none unless recommendations.any?
    recommendations.map(&:first)
  end


  private
  #Recommendations are computed by actual user upvotes and
  #Matching with other users upvoted books that belongs to the same genre
  #If there is any, similarity factor computed by
  #Dividing commong books with all books that other user upvoted
  #After we check common books for every user
  #We're sorting books by similarity factor
  def recommendations
    @recommendations ||= begin
      Hash.new(0).tap do |recommending|
        users.find_each do |user|
          user_books = user.upvoted_books
          liked_books = user_books.joins(:genres).where(genres: {id: liked_genre_ids})

          next unless liked_books.exists?

          common_books = upvoted_books & liked_books

          similarity = common_books.count.to_f / user_books.count

          liked_books.where.not(id: upvoted_books.ids).find_each do |book|
            recommending[book] += similarity
          end
        end
        recommending.sort_by{|k, v| -v}
      end
    end
  end

  def upvoted_books
    @upvoted_books ||= @user.upvoted_books
  end

  def liked_genre_ids
    @liked_genres ||= upvoted_books.joins(:genres).pluck('genres.id')
  end

  def users
    @users ||= User.where.not(id: @user.id)
  end
end
