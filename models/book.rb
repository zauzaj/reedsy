class Book < ActiveRecord::Base
  belongs_to :author
  has_many :upvotes
  has_and_belongs_to_many :genres
end