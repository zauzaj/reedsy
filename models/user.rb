class User < ActiveRecord::Base
  has_many :upvotes
  has_many :upvoted_books, through: :upvotes, source: :book
  has_many :follows
  has_many :followed_authors, through: :follows, source: :author
end