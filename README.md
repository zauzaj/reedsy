## Instructions

There are 5 folders each represents answer to concrete question accordingly.
- q1
- q2
- q3
- q4
- q5

In order to setup project, run `bundle install`  and then
`RAILS_ENV=test rake db:drop db:create db:migrate`

You can now run specs with
`rspec spec/`
