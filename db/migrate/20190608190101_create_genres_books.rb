class CreateGenresBooks < ActiveRecord::Migration[5.2]
  def change
    create_join_table :books, :genres do |t|
      t.index [:book_id, :genre_id]
    end
  end
end
