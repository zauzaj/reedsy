class CreateFollows < ActiveRecord::Migration[5.2]
  def change
    create_table :follows do |t|
      t.belongs_to :user
      t.belongs_to :author

      t.timestamps
    end
  end
end
