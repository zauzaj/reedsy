class BaseProvider
  def self.run!(params={})
    new(params).run!
  end

  def initialize(params)
    @params = params
  end

  def run!
    raise NotImplementedError
  end
end