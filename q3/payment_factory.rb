class PaymentFactory
  class InvalidPaymentFactoryError < StandardError;end

  def self.call(factory, params={})
    new(factory, params).call
  end

  def initialize(factory, params)
    @factory, @params = factory, params
  end

  def call
    adapter.run!
  end

  def adapter
    @adapter ||= "#{@factory.to_s.camelcase}".constantize
  rescue
    raise InvalidPaymentFactoryError
  end
end