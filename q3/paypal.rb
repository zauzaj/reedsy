require_relative './base_provider'

class PayPal < BaseProvider
  def run!
    {
      message: 'PayPal provider run!',
      klass:   self.class,
      payment_type: :paypal,
      params:  @params
    }
  end
end