require_relative './base_provider'

class Stripe < BaseProvider
  def run!
    {
      message: 'Stripe provider run!',
      klass:   self.class,
      payment_type: :stripe,
      params:  @params
    }
  end
end