require_relative '../spec_helper'

describe PayPal do
  context '.run!' do
    let(:payment_factory) { PayPal }

    it 'initialize PayPal and call run!' do
      expect_any_instance_of(PayPal).to receive(:run!)
      PayPal.run!
    end

    context '#response' do
      let(:params) {
        {
          invoice_settings: {
            custom_fields: nil,
            default_payment_method: 'credit card',
            footer: nil,
          }
        }
      }
      before(:all) {
        @res = PayPal.run!
      }
      it 'contains message, klass, params keys' do
        expect(@res.keys).to contain_exactly(*[:message, :klass, :params, :payment_type])
      end

      it 'returns proper message' do
        expect(@res[:message]).to eq 'PayPal provider run!'
      end

      it 'returns proper payment_type' do
        expect(@res[:payment_type]).to eq :paypal
      end

      it 'params contains provided data' do
        @res = PayPal.run!(params)
        expect(@res[:params]).to eq params
      end
    end
  end
end