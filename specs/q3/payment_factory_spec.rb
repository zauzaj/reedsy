require_relative '../spec_helper'

describe PaymentFactory do
  context '.call' do
    let(:payment_factory) { :stripe }
    it 'initialize PaymentFactory' do
      expect_any_instance_of(PaymentFactory).to receive(:call)
      PaymentFactory.call(payment_factory)
    end

    it 'call Stripe.run!' do
      expect(Stripe).to receive(:run!)
      PaymentFactory.call(payment_factory)
    end

    it 'respond with wrong payment method' do
      expect do
        PaymentFactory.call(:test)
      end.to raise_error PaymentFactory::InvalidPaymentFactoryError
    end
  end
end