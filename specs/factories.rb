FactoryBot.define do
  factory :user do
    name { "#{Faker::Name.first_name} #{Faker::Name.last_name}" }
  end
end

FactoryBot.define do
  factory :author do
    name { "#{Faker::Name.first_name} #{Faker::Name.last_name}" }
  end
  trait :with_books do
    after :create do |author|
      FactoryBot.create_list :book, 3, :author => author
    end
  end
end

FactoryBot.define do
  factory :book do
    title { Faker::Book.title}
    author
    genres { [FactoryBot.create(:genre)] }
    published_on { Faker::Date.between(10.days.ago, Date.today) }

  end
end

FactoryBot.define do
  factory :genre do
  sequence(:title) {|n| "#{n+1}-genre" }
  end
end