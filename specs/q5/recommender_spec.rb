require_relative '../spec_helper'

describe Recommender do
  let(:user)     { FactoryBot.create(:user) }
  let!(:author1) { FactoryBot.create(:author, :with_books)}
  let!(:author2) { FactoryBot.create(:author, :with_books)}
  let!(:author3) { FactoryBot.create(:author, :with_books)}
  context '.call' do
    it 'makes new instance of Recommender' do
      expect_any_instance_of(Recommender).to receive(:call)
      Recommender.call(user)
    end
  end

  context '.recommendations' do
    let(:another_user) { create(:user) }
    context 'no upvotes' do
      it 'returns empty array' do
        books = Recommender.new(user).call
        expect(books).to be_empty
      end
    end
    context 'with upvotes' do
      let(:recommender) { Recommender.new(user).send(:recommendations) }
      let(:favorite_genres) { user.upvoted_books.flat_map(&:genres) }
      before(:each) do
        user.upvoted_books = [author1.books.first, author2.books.first]
        another_user.upvoted_books = [author1.books.first, author3.books.last]
      end
      context 'no genres match' do
        it 'return hash response' do
          expect(recommender).to be_instance_of Hash
        end

        it 'has one recommended book' do
          expect(recommender).to be_empty
        end
      end
      context 'genre match' do
        before(:each) do
          @recommendation1= author3.books.last
          @recommendation1.genres = [favorite_genres.first]
        end
        it 'return hash response' do
          expect(recommender).to be_instance_of Hash
        end

        it 'has one recommended book' do
          expect(recommender.count).to eq 1
        end

        it 'recommends author3.books.last' do
          expect(recommender.keys.first).to eq @recommendation1
        end

        it 'returns 0.5 similariry' do
          expect(recommender[@recommendation1]).to eq 1.0/another_user.upvoted_books.count
        end
        context '#add one more common book' do
          before(:each) do
            another_user.upvoted_books << author2.books.first
          end

          it 'returns new higher similarity - 0.66' do
            expect(recommender[@recommendation1]).to eq 2.0/another_user.upvoted_books.count
          end

          context '#another user with similarity' do
            let(:user2) { create(:user) }
            before(:each) do
              @recommendation2 = author2.books.last
              @recommendation2.genres = [favorite_genres.last]
              user2.upvoted_books = author2.books
            end

            it 'returns 2 recommendations' do
              expect(recommender.count).to eq 2
            end

            it 'returns 0.5 similariry' do
              expect(recommender[@recommendation2]).to eq 1.0/user2.upvoted_books.count
            end

            it 'retuns @recommendation1 & @recommendation1 in hash' do
              expect(recommender.keys).to contain_exactly(*[@recommendation1, @recommendation2])
            end

            it 'returns recommendations ordered by similarity' do
              expect(recommender.keys.first).to eq @recommendation1
              expect(recommender.values.first).to eq 2.0/another_user.upvoted_books.count
            end

            it 'should return @recommendation1, @recomendation2 in exact order' do
              expect(Recommender.call(user)).to eq [@recommendation1, @recommendation2]
            end
          end
        end
      end
    end
  end
end