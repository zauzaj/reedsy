require_relative '../spec_helper'

describe FeedGenerator do
  subject { described_class.new(user) }
  let(:user)                 { FactoryBot.create(:user) }
  let(:another_user)         { FactoryBot.create(:user)}
  let!(:followed_author)     { FactoryBot.create(:author, :with_books) }
  let!(:non_followed_author) { FactoryBot.create(:author, :with_books) }

  describe '#initialize' do
    it { expect(subject.instance_variable_get(:@user)).to_not be_nil }
  end

  describe '.retrieve' do
    before(:each) do
      #Simulate where there is other user with his feeds
      another_user.followed_authors << non_followed_author
      another_user.upvoted_books << followed_author.books.last
    end

    context '#no follows & no upvotes' do
      it 'returns empty array' do
        expect(subject.retrieve).to be_empty
      end
    end

    context 'with follows' do
      before(:each) do
        user.followed_authors << followed_author
      end

      it 'return non empty array' do
        expect(subject.retrieve).to_not be_empty
      end

      it 'return all books from followed_author' do
        expect(subject.retrieve).to contain_exactly(*followed_author.books)
      end

      it 'not returns books from non_followed_author' do
        expect(subject.retrieve).to_not include(*non_followed_author.books)
      end
    end

    context '#with upvotes' do
      before(:each) do
        @book = non_followed_author.books.first
        user.upvoted_books << @book
      end

      it 'return only one book' do
        expect(subject.retrieve.size).to eq 1
      end

      it 'return only upvoted book' do
        expect(subject.retrieve).to contain_exactly(*[@book])
      end
    end

    context '#upvoted_books & followed_authors' do
      before(:each) do
        user.followed_authors << followed_author
        @book = non_followed_author.books.first
        user.upvoted_books << @book
      end

      it 'returns 4 books' do
        expect(subject.retrieve.size).to eq 4
      end

      it 'returns all books from followed_author & upvoted book' do
        expect(subject.retrieve).to contain_exactly(*[followed_author.books, @book].flatten)
      end
    end

    context '#ordering' do
      before(:each) do
        user.followed_authors << followed_author
        @book = non_followed_author.books.first
        user.upvoted_books << @book
      end

      it 'keeps @book first as last has been upvoted for' do
        expect(subject.retrieve.first).to eq @book
      end

      it 'keeps @book1 first as last has been upvoted for' do
        @book1 = followed_author.books.last
        user.upvoted_books << @book1
        expect(subject.retrieve.first).to eq @book1
      end

      it 'keeps followed author books first' do
        u = user.upvotes.first
        u.update(created_at: -2.hours.from_now)
        expect(subject.retrieve.first(3)).to contain_exactly(*followed_author.books)
      end
    end
  end

  describe '.refresh' do
    before(:each) do
      user.followed_authors << followed_author
      @book = non_followed_author.books.first
      user.upvoted_books << @book
      #Simulate old follows & upvotes creation time
      user.follows.update_all(created_at: -2.hours.from_now)
      user.upvotes.update_all(created_at: -10.hours.from_now)
    end

    context 'new books' do
      let(:feeder) { FeedGenerator.new(user) }
      let!(:new_author) { FactoryBot.create(:author, :with_books) }
      before(:each) do
        displayed_books = subject.retrieve
        @latest_book = displayed_books.first
        user.followed_authors << new_author
      end

      it 'returns only new books' do
        expect(feeder.refresh(@latest_book.id)).to contain_exactly(*new_author.books)
      end

      it 'returns new books in correct order' do
        b = create(:book, author: non_followed_author)
        user.upvoted_books << b
        b.upvotes.update_all(created_at: 2.hours.from_now)
        expect(feeder.refresh(@latest_book.id)).to eq [b, new_author.books].flatten
      end

      it 'returns new book from the already followed user in correct order' do
        b = create(:book, author: followed_author)
        user.upvoted_books << b
        b.upvotes.update_all(created_at: 2.hours.from_now)
        expect(feeder.refresh(@latest_book.id)).to eq [b, new_author.books].flatten
      end
    end

  end
end