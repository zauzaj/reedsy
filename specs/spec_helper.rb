require 'active_record'
require 'database_cleaner'
require 'factory_bot'
require 'pry-rails'
require 'faker'
require 'shoulda-matchers'
require_relative 'factories'

Dir[Dir.pwd + '/q*/*.rb'].each { |file| require file }
Dir[Dir.pwd + '/models/*.rb'].each { |file| require file }

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: Dir.pwd + '/db/reedsy_test.sqlite3'
)

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :active_record
  end
end

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.find_definitions
  end

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
    DatabaseCleaner.strategy = :transaction
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end