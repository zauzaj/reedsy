require_relative '../spec_helper'

describe Author, type: :model do
  it { should have_many :books }
  it { should have_many :follows }
end