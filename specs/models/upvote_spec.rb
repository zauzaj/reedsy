require_relative '../spec_helper'

describe Upvote, type: :model do
  it { should belong_to :user }
  it { should belong_to :book }
end