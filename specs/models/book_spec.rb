require_relative '../spec_helper'

describe Book, type: :model do
  it { should belong_to :author }
  it { should have_many :upvotes }
  it { should have_and_belong_to_many :genres }
end