require_relative '../spec_helper'

describe Follow, type: :model do
  it { should belong_to :user }
  it { should belong_to :author }
end