require_relative '../spec_helper'

describe User, type: :model do
  it { should have_many :upvotes }
  it { should have_many :follows }
  it { should have_many :followed_authors }
  it { should have_many :upvoted_books }
end