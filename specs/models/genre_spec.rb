require_relative '../spec_helper'

describe Genre, type: :model do
  it { should have_and_belong_to_many :books }
end