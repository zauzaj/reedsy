## About me

I'm Bojan, software engineer from Serbia. I started programming when I was 14 and since then I'm mostly in front of my laptop. I enjoy spending time in nature with animals and going in long walks with my wife. In my free time I voluntary work at animal rescue center, where I care of abandoned animals and helping their adoption.
Major achievement in my career, I would say, was founding GospelBuddy (ex. LoveRealm), https://gospelbuddy.com, where I was technical lead and co-founder. I developed MVP and deployed first product version after what we hired more developers ( FrontEnd, Mobile, BackEnd) so I started leading team of 5.

## Professional debrief

So far I've been involved on dozens of projects, mostly highly dynamic startups, where I've gained great experience delivering stable and quality code under different circumstances.
Some big projects I've been involved, as Senior Engineer, are
 - https://fra.me
 - https://buddy.com
 - https://www.onea.se
 - https://gospelbuddy.com
 - https://keldoc.com
 

I mostly took part on implementing architectures and infrastructures, and also designing Resftul Apis. I've mainly used Ruby but also have been worked with ReactJS and GoLang. I also have big experience with TDD and writing test for the sake of code stability and maintainability.

Currently I'm working as Lead Backend Engineer at Keldoc, online booking platform for doctors.
I'm responsible for
- *software design* (multiple business units split on different Rails engines, cleaning models, endpoints, introducing different patterns, dealing with legacy etc)
- *app performances* (rewriting complex Postgres queries and speeding up some highly used features)
- *tests design* keeping tests clean, readable and fast (reduced CI execution from 10min to ~3min)
